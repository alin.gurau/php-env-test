#!/usr/bin/env bash
START=$(date +%s)

# Turn command echoing on
set -x

# Add required repositories
#add-apt-repository ppa:chris-lea/node.js -y

apt-get update

# Remove unused libraries after changing the GuestAdditions versions on your host
apt-get -y autoremove

# Locale settings
set +x && echo 'Locale settings...' && set -x
locale-gen en_US.UTF-8 ro_RO.UTF-8
dpkg-reconfigure locales
set +x && echo -e 'OK\n...............................' && set -x

# Apache Install
set +x && echo 'Installing Apache...' && set -x
apt-get install -y apache2
set +x && echo -e 'OK\n...............................' && set -x

# Create the file Apache config file
set +x && echo 'Configuring Apache...' && set -x

# Check to see if the vendor folder is in the project already
#if [ ! -d "/srv/www/vhosts/$1.local/$1/vendor" ]; then
    # Install the Symfony installer
#    set +x && echo 'Install the Symfony installer...' && set -x
#    curl -LsS http://symfony.com/installer -o /usr/local/bin/symfony
#    chmod a+x /usr/local/bin/symfony

    # Install Symfony
#   symfony new /srv/www/vhosts/$1.local/$1
#fi

# Create Symfony folders
#mkdir /srv/www/vhosts/$1.dev/$1/app/logs
#mkdir /srv/www/vhosts/$1.dev/$1/app/cache
#mkdir /srv/www/vhosts/$1.dev/$1/web
#cd /srv/www/vhosts/$1.dev/$1
#chmod -R 777 web 
#chmod -R 777 app/logs
#chmod -R 777 app/cache

# New virtual site
cd /etc/apache2/sites-available
touch $1.local.conf
cat <<EOF >> $1.local.conf
<VirtualHost *:80>
    ServerName $1.local

    DocumentRoot /srv/www/vhosts/$1.local/$1/public
    <Directory /srv/www/vhosts/$1.local/$1/public>
        # enable the .htaccess rewrites
        AllowOverride All
        Order allow,deny
        allow from all
	Require all granted
    </Directory>

    ErrorLog /srv/www/vhosts/$1.local/logs/error.log
    CustomLog /srv/www/vhosts/$1.local/logs/access.log combined
</VirtualHost>
EOF

# enable site
a2ensite $1.local.conf

# disable defaul site
a2dissite 000-default

# activate mode rewrite
a2enmod rewrite

set +x && echo -e 'OK\n...............................' && set -x

# MySQL Install
set +x && echo 'Installing MySQL...' && set -x
debconf-set-selections <<< 'mysql-server mysql-server/root_password password '$2
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password '$2
apt-get -y install mysql-server
set +x && echo -e 'OK\n...............................' && set -x

# PHP Install
set +x && echo 'Installing PHP...' && set -x
apt-get install -y php7.2 php-gd php-curl php-mysql
set +x && echo -e 'OK\n...............................' && set -x

# PHP Configuration
set +x && echo 'Configuring PHP...' && set -x
sed -i "s/;realpath_cache_size = 16k/realpath_cache_size = 4096k/" /etc/php7.2/apache2/php.ini
sed -i "s/;realpath_cache_size = 16k/realpath_cache_size = 4096k/" /etc/php7.2/cli/php.ini
sed -i "s/;realpath_cache_ttl = 120/realpath_cache_ttl = 7200/" /etc/php7.2/apache2/php.ini
sed -i "s/;realpath_cache_ttl = 120/realpath_cache_ttl = 7200/" /etc/php7.2/cli/php.ini
sed -i "s/upload_max_filesize = 2M/upload_max_filesize = 10M/" /etc/php7.2/apache2/php.ini
sed -i "s/upload_max_filesize = 2M/upload_max_filesize = 10M/" /etc/php7.2/cli/php.ini
sed -i "s/short_open_tag = On/short_open_tag = Off/" /etc/php7.2/apache2/php.ini
sed -i "s/short_open_tag = On/short_open_tag = Off/" /etc/php7.2/cli/php.ini
sed -i "s/;date.timezone =/date.timezone = Europe\/Bucharest/" /etc/php7.2/apache2/php.ini
sed -i "s/;date.timezone =/date.timezone = Europe\/Bucharest/" /etc/php7.2/cli/php.ini
sed -i "s/memory_limit = 128M/memory_limit = 256M/" /etc/php7.2/apache2/php.ini
sed -i "s/memory_limit = 128M/memory_limit = 256M/" /etc/php7.2/cli/php.ini
sed -i "s/_errors = Off/_errors = On/" /etc/php7.2/apache2/php.ini
sed -i "s/_errors = Off/_errors = On/" /etc/php7.2/cli/php.ini
echo "xdebug.default_enable=1
xdebug.remote_enable=1
xdebug.remote_handler=dbgp
xdebug.remote_host=$4 #VERY IMPORTANT, HERE THE IP USED BY VAGRANT FOR THE HOST
xdebug.remote_port=9000
xdebug.remote_autostart=0
xdebug.remote_connect_back = 1
xdebug.remote_log=/tmp/php7.2-xdebug.log" >> /etc/php7.2/apache2/php.ini

set +x && echo -e 'OK\n...............................' && set -x

# PHPMyAdmin Install
set +x && echo 'Installing PHPMyAdmin...'  && set -x
debconf-set-selections <<< 'phpmyadmin phpmyadmin/app-password-confirm password '
debconf-set-selections <<< 'phpmyadmin phpmyadmin/dbconfig-install boolean true'
debconf-set-selections <<< 'phpmyadmin phpmyadmin/mysql/admin-pass password '$2
debconf-set-selections <<< 'phpmyadmin phpmyadmin/mysql/app-pass password '
debconf-set-selections <<< 'phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2'
apt-get install -y phpmyadmin
set +x && echo -e 'OK\n...............................' && set -x

# Install some tools
set +x && echo 'Installing some tools...' && set -x
#apt-get -y install mc
apt-get -y install git
set +x && echo -e 'OK\n...............................' && set -x

# Install and config for ACL
set +x && echo 'Installing and configuring for ACL...' && set -x
apt-get -y install acl
sed -i "s/defaults/defaults,acl/" /etc/fstab
mount -o remount /
mount | grep acl
set +x && echo -e 'OK\n...............................' && set -x

# Ruby Install
#set +x && echo 'Ruby version...' && set -x
#ruby -v
#which ruby
#echo "gem: --no-ri --no-rdoc" > ~/.gemrc
#set +x && echo -e 'OK\n...............................' && set -x

# RubyGems Install
#set +x && echo 'Upgrade RubyGems version...' && set -x
#gem -v
#wget http://production.cf.rubygems.org/rubygems/rubygems-update-2.3.0.gem
#gem install rubygems-update-2.3.0
#update_rubygems
#gem -v
#set +x && echo -e 'OK\n...............................' && set -x

# Compass and Uglify JS/CSS Install
#set +x && echo 'Installing Node, NPM, Uglify JS/CSS...' && set -x
#apt-get -y install python-software-properties python g++ make nodejs
#ln -s /usr/bin/nodejs /usr/bin/node
#set +x && echo 'Node version : ' && set -x
#node -v
#set +x && echo 'NPM version : ' && set -x
#npm -v
#set +x && echo 'Upgrade NPM...' && set -x
#npm install -g npm
#set +x && echo 'New NPM version : ' && set -x
#npm -v
# If for some reason, if you see npm is not installed, you may try running :
# apt-get -y install npm
#apt-get -y install node-uglify
#npm install -g uglify-js
#npm install -g uglifycss
#ln -s /usr/bin/uglifycss /usr/local/bin/uglifycss
#ln -s /usr/bin/uglifyjs /usr/local/bin/uglifyjs
#
#set +x && echo 'Your path uglify is : ' && set -x
#npm bin -g
#set +x && echo -e 'OK\n...............................' && set -x


# Setting up permissions in Symfony
#set +x && echo 'Setting up permissions in Symfony...' && set -x
#cd /srv/www/vhosts/$1.dev 
#HTTPDUSER=`ps aux | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
#setfacl -R -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX app/cache app/logs web
#setfacl -dR -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX app/cache app/logs web
#set +x && echo -e 'OK\n...............................' && set -x

# Check Symfony requirements
#set +x && echo 'Check Symfony requirements...' && set -x
#cd /srv/www/vhosts/$1.dev
#php composer.phar self-update
#COMPOSER_PROCESS_TIMEOUT=5000 php composer.phar install
# Install the latest version of front dependencies ie: Foundation, jQuery, etc.
#bower install --allow-root
#php app/check.php
#set +x && echo -e 'OK\n............................... ' && set -x

# Load database
#cd /srv/www/vhosts/$1.dev/database
#php load-db.php
#set +x && echo -e 'OK\n............................... ' && set -x

# Just a reminder, before deploying your code in production, don’t forget to optimize the autoloader:
#composer dump-autoload --optimize
# This can also be used while installing packages with the --optimize-autoloader option. 
# Without that optimization, you may notice a performance loss from 20 to 25%.

END=$(date +%s)
DIFF=$(( $END - $START ))
echo -e 'Done! It took $DIFF seconds to execute these commands!\n...............................'